"use strict";
let passwordForms = document.forms.password;
passwordForms.addEventListener("click", (event) => {
  if (event.target.closest("i")) {
    event.target.closest("label").children[2].classList.toggle("noactiv");
    event.target.closest("label").children[1].classList.toggle("noactiv");
    if (
      event.target.closest("label").children[1].classList.contains("noactiv")
    ) {
      event.target.closest("label").children[0].type = "text";
    } else {
      event.target.closest("label").children[0].type = "password";
    }
  }
  let inPassword = passwordForms.inPassword.value;
  let rePassword = passwordForms.rePassword.value;
  if (event.target.closest("button")) {
    event.preventDefault();
    if (inPassword !== rePassword) {
      alert("Потрібно ввести однакові значення");
    } else if (inPassword.trim() === "" || rePassword.trim() === "") {
      alert("Потрібно ввести значення");
    } else {
      alert("You are welcome");
    }
  }
});
